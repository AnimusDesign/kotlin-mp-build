FROM centos:latest

ENV SDKMAN_DIR=/root/.sdkman

# update the image
RUN yum -y upgrade

# install requirements, install and configure sdkman
# see https://sdkman.io/usage for configuration options
RUN yum -y install curl ca-certificates zip unzip openssl which findutils && \
    update-ca-trust && \
    curl -s "https://get.sdkman.io" | bash && \
    echo "sdkman_auto_answer=true" > $SDKMAN_DIR/etc/config && \
    echo "sdkman_auto_selfupdate=false" >> $SDKMAN_DIR/etc/config

# Source sdkman to make the sdk command available and install candidate

# Add candidate path to $PATH environment variable
ENTRYPOINT ["/bin/bash", "-c", "source $SDKMAN_DIR/bin/sdkman-init.sh && \"$@\"", "-s"]

RUN bash -c "source $SDKMAN_DIR/bin/sdkman-init.sh && sdk install java 21.0.0.2.r11-grl"
RUN bash -c "source $SDKMAN_DIR/bin/sdkman-init.sh && sdk install java 11.0.10.hs-adpt"
RUN bash -c "source $SDKMAN_DIR/bin/sdkman-init.sh && sdk install kotlin"
RUN bash -c "source $SDKMAN_DIR/bin/sdkman-init.sh && sdk install kscript"

ENV JAVA_HOME="$SDKMAN_DIR/candidates/java/current"
ENV PATH="$SDKMAN_DIR/candidates/java/11.0.10.hs-adpt/bin:$SDKMAN_DIR/candidates/java/21.0.0.2.r11-grl/bin:$JAVA_HOME/bin:$SDKMAN_DIR/bin:$PATH"
RUN source $SDKMAN_DIR/bin/sdkman-init.sh 

RUN bash -c "source $SDKMAN_DIR/bin/sdkman-init.sh && gu install native-image"
RUN yum install -y nodejs npm openssh openssh-clients git
RUN yum install -y tree wget curl
RUN yum install -y python38 python38-devel ncurses-devel
RUN yum install -y autoconf automake binutils bison flex gcc gcc-c++ gettext libtool make patch pkgconfig redhat-rpm-config rpm-build rpm-sign ctags elfutils patchutils ncurses ncurses-c++-libs ncurses-compat-libs ncurses-devel ncurses-libs
RUN yum install -y yum-utils; \
	yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo; \
	yum install -y docker-ce docker-ce-cli containerd.io;
CMD ["bash"]
